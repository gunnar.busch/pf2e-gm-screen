This is GM Screen for PF2e Game System that uses Journal Entries.

After enabling the module, import '!GM Screen' from the compendium pack.

![](preview.jpg)

Special thanks to Nerull for Action Tables and ArthurTrumpet for the macro!

If you have any suggestions/found any errors, please DM me at discord(>.tre_#7698) or create an issue.


## License
This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](https://creativecommons.org/licenses/by-nc/4.0/)
This work is licensed under Foundry Virtual Tabletop [EULA - Limited License Agreement for module development v 0.1.6](http://foundryvtt.com/pages/license.html).
